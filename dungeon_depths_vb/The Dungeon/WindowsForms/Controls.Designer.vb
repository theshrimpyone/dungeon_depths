﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Controls
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Controls))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtUp = New System.Windows.Forms.TextBox()
        Me.txtDown = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtLeft = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtRight = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMagic = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtAttack = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtTalk = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtInpect = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtWear2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtWear1 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtShop = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtUse = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtDrink = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtRun = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtWait = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtSpec = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtYes = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtEat = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtSInspect = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtEquip = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtNo = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtIInspect = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtSelectionKey11 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtSelectionKey10 = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtSelectionKey9 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtSelectionKey8 = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtSelectionKey7 = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtSelectionKey6 = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtSelectionKey5 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtSelectionKey4 = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtSelectionKey3 = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtSelectionKey2 = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtSelectionKey1 = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(226, 624)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 28)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Save"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(13, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(144, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Move up ---------"
        '
        'txtUp
        '
        Me.txtUp.BackColor = System.Drawing.Color.Black
        Me.txtUp.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtUp.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUp.ForeColor = System.Drawing.Color.White
        Me.txtUp.Location = New System.Drawing.Point(173, 20)
        Me.txtUp.MaxLength = 1
        Me.txtUp.Name = "txtUp"
        Me.txtUp.Size = New System.Drawing.Size(129, 20)
        Me.txtUp.TabIndex = 3
        '
        'txtDown
        '
        Me.txtDown.BackColor = System.Drawing.Color.Black
        Me.txtDown.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDown.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDown.ForeColor = System.Drawing.Color.White
        Me.txtDown.Location = New System.Drawing.Point(173, 46)
        Me.txtDown.MaxLength = 1
        Me.txtDown.Name = "txtDown"
        Me.txtDown.Size = New System.Drawing.Size(129, 20)
        Me.txtDown.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(13, 47)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Move down -------"
        '
        'txtLeft
        '
        Me.txtLeft.BackColor = System.Drawing.Color.Black
        Me.txtLeft.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtLeft.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLeft.ForeColor = System.Drawing.Color.White
        Me.txtLeft.Location = New System.Drawing.Point(173, 72)
        Me.txtLeft.MaxLength = 1
        Me.txtLeft.Name = "txtLeft"
        Me.txtLeft.Size = New System.Drawing.Size(129, 20)
        Me.txtLeft.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(13, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(144, 17)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Move left -------"
        '
        'txtRight
        '
        Me.txtRight.BackColor = System.Drawing.Color.Black
        Me.txtRight.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRight.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRight.ForeColor = System.Drawing.Color.White
        Me.txtRight.Location = New System.Drawing.Point(173, 98)
        Me.txtRight.MaxLength = 1
        Me.txtRight.Name = "txtRight"
        Me.txtRight.Size = New System.Drawing.Size(129, 20)
        Me.txtRight.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(13, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(144, 17)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Move right ------"
        '
        'txtMagic
        '
        Me.txtMagic.BackColor = System.Drawing.Color.Black
        Me.txtMagic.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMagic.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMagic.ForeColor = System.Drawing.Color.White
        Me.txtMagic.Location = New System.Drawing.Point(173, 202)
        Me.txtMagic.MaxLength = 1
        Me.txtMagic.Name = "txtMagic"
        Me.txtMagic.Size = New System.Drawing.Size(129, 20)
        Me.txtMagic.TabIndex = 18
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(13, 203)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(144, 17)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Magic -----------"
        '
        'txtAttack
        '
        Me.txtAttack.BackColor = System.Drawing.Color.Black
        Me.txtAttack.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtAttack.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAttack.ForeColor = System.Drawing.Color.White
        Me.txtAttack.Location = New System.Drawing.Point(173, 176)
        Me.txtAttack.MaxLength = 1
        Me.txtAttack.Name = "txtAttack"
        Me.txtAttack.Size = New System.Drawing.Size(129, 20)
        Me.txtAttack.TabIndex = 16
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(13, 177)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(144, 17)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Attack ----------"
        '
        'txtTalk
        '
        Me.txtTalk.BackColor = System.Drawing.Color.Black
        Me.txtTalk.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtTalk.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTalk.ForeColor = System.Drawing.Color.White
        Me.txtTalk.Location = New System.Drawing.Point(173, 150)
        Me.txtTalk.MaxLength = 1
        Me.txtTalk.Name = "txtTalk"
        Me.txtTalk.Size = New System.Drawing.Size(129, 20)
        Me.txtTalk.TabIndex = 14
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(13, 151)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(144, 17)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Talk to NPC -----"
        '
        'txtInpect
        '
        Me.txtInpect.BackColor = System.Drawing.Color.Black
        Me.txtInpect.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtInpect.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInpect.ForeColor = System.Drawing.Color.White
        Me.txtInpect.Location = New System.Drawing.Point(173, 124)
        Me.txtInpect.MaxLength = 1
        Me.txtInpect.Name = "txtInpect"
        Me.txtInpect.Size = New System.Drawing.Size(129, 20)
        Me.txtInpect.TabIndex = 12
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(13, 125)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(144, 17)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Interact --------"
        '
        'txtWear2
        '
        Me.txtWear2.BackColor = System.Drawing.Color.Black
        Me.txtWear2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWear2.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWear2.ForeColor = System.Drawing.Color.White
        Me.txtWear2.Location = New System.Drawing.Point(173, 409)
        Me.txtWear2.MaxLength = 1
        Me.txtWear2.Name = "txtWear2"
        Me.txtWear2.Size = New System.Drawing.Size(129, 20)
        Me.txtWear2.TabIndex = 34
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(13, 410)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(144, 17)
        Me.Label9.TabIndex = 33
        Me.Label9.Text = "Wear (Other) ----"
        '
        'txtWear1
        '
        Me.txtWear1.BackColor = System.Drawing.Color.Black
        Me.txtWear1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWear1.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWear1.ForeColor = System.Drawing.Color.White
        Me.txtWear1.Location = New System.Drawing.Point(173, 383)
        Me.txtWear1.MaxLength = 1
        Me.txtWear1.Name = "txtWear1"
        Me.txtWear1.Size = New System.Drawing.Size(129, 20)
        Me.txtWear1.TabIndex = 32
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(13, 384)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(144, 17)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Wear (Armor) ----"
        '
        'txtShop
        '
        Me.txtShop.BackColor = System.Drawing.Color.Black
        Me.txtShop.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtShop.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShop.ForeColor = System.Drawing.Color.White
        Me.txtShop.Location = New System.Drawing.Point(173, 357)
        Me.txtShop.MaxLength = 1
        Me.txtShop.Name = "txtShop"
        Me.txtShop.Size = New System.Drawing.Size(129, 20)
        Me.txtShop.TabIndex = 30
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(13, 358)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(144, 17)
        Me.Label11.TabIndex = 29
        Me.Label11.Text = "Shop ------------"
        '
        'txtUse
        '
        Me.txtUse.BackColor = System.Drawing.Color.Black
        Me.txtUse.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtUse.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUse.ForeColor = System.Drawing.Color.White
        Me.txtUse.Location = New System.Drawing.Point(173, 331)
        Me.txtUse.MaxLength = 1
        Me.txtUse.Name = "txtUse"
        Me.txtUse.Size = New System.Drawing.Size(129, 20)
        Me.txtUse.TabIndex = 28
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(13, 332)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(144, 17)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Use -------------"
        '
        'txtDrink
        '
        Me.txtDrink.BackColor = System.Drawing.Color.Black
        Me.txtDrink.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtDrink.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDrink.ForeColor = System.Drawing.Color.White
        Me.txtDrink.Location = New System.Drawing.Point(173, 305)
        Me.txtDrink.MaxLength = 1
        Me.txtDrink.Name = "txtDrink"
        Me.txtDrink.Size = New System.Drawing.Size(129, 20)
        Me.txtDrink.TabIndex = 26
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(13, 306)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(144, 17)
        Me.Label13.TabIndex = 25
        Me.Label13.Text = "Drink -----------"
        '
        'txtRun
        '
        Me.txtRun.BackColor = System.Drawing.Color.Black
        Me.txtRun.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRun.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRun.ForeColor = System.Drawing.Color.White
        Me.txtRun.Location = New System.Drawing.Point(173, 279)
        Me.txtRun.MaxLength = 1
        Me.txtRun.Name = "txtRun"
        Me.txtRun.Size = New System.Drawing.Size(129, 20)
        Me.txtRun.TabIndex = 24
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(13, 280)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(144, 17)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "Run (Combat) ----"
        '
        'txtWait
        '
        Me.txtWait.BackColor = System.Drawing.Color.Black
        Me.txtWait.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWait.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWait.ForeColor = System.Drawing.Color.White
        Me.txtWait.Location = New System.Drawing.Point(173, 253)
        Me.txtWait.MaxLength = 1
        Me.txtWait.Name = "txtWait"
        Me.txtWait.Size = New System.Drawing.Size(129, 20)
        Me.txtWait.TabIndex = 22
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(13, 254)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(144, 17)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Wait ------------"
        '
        'txtSpec
        '
        Me.txtSpec.BackColor = System.Drawing.Color.Black
        Me.txtSpec.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSpec.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpec.ForeColor = System.Drawing.Color.White
        Me.txtSpec.Location = New System.Drawing.Point(173, 227)
        Me.txtSpec.MaxLength = 1
        Me.txtSpec.Name = "txtSpec"
        Me.txtSpec.Size = New System.Drawing.Size(129, 20)
        Me.txtSpec.TabIndex = 20
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(13, 228)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(144, 17)
        Me.Label16.TabIndex = 19
        Me.Label16.Text = "Special ---------"
        '
        'txtYes
        '
        Me.txtYes.BackColor = System.Drawing.Color.Black
        Me.txtYes.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtYes.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtYes.ForeColor = System.Drawing.Color.White
        Me.txtYes.Location = New System.Drawing.Point(173, 535)
        Me.txtYes.MaxLength = 1
        Me.txtYes.Name = "txtYes"
        Me.txtYes.Size = New System.Drawing.Size(129, 20)
        Me.txtYes.TabIndex = 42
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(13, 536)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(144, 17)
        Me.Label17.TabIndex = 41
        Me.Label17.Text = "Yes -------------"
        '
        'txtEat
        '
        Me.txtEat.BackColor = System.Drawing.Color.Black
        Me.txtEat.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtEat.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEat.ForeColor = System.Drawing.Color.White
        Me.txtEat.Location = New System.Drawing.Point(173, 510)
        Me.txtEat.MaxLength = 1
        Me.txtEat.Name = "txtEat"
        Me.txtEat.Size = New System.Drawing.Size(129, 20)
        Me.txtEat.TabIndex = 40
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(13, 510)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(144, 17)
        Me.Label18.TabIndex = 39
        Me.Label18.Text = "Eat -------------"
        '
        'txtSInspect
        '
        Me.txtSInspect.BackColor = System.Drawing.Color.Black
        Me.txtSInspect.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSInspect.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSInspect.ForeColor = System.Drawing.Color.White
        Me.txtSInspect.Location = New System.Drawing.Point(173, 460)
        Me.txtSInspect.MaxLength = 1
        Me.txtSInspect.Name = "txtSInspect"
        Me.txtSInspect.Size = New System.Drawing.Size(129, 20)
        Me.txtSInspect.TabIndex = 38
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(13, 461)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(144, 17)
        Me.Label19.TabIndex = 37
        Me.Label19.Text = "Self Inspect ----"
        '
        'txtEquip
        '
        Me.txtEquip.BackColor = System.Drawing.Color.Black
        Me.txtEquip.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtEquip.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEquip.ForeColor = System.Drawing.Color.White
        Me.txtEquip.Location = New System.Drawing.Point(173, 434)
        Me.txtEquip.MaxLength = 1
        Me.txtEquip.Name = "txtEquip"
        Me.txtEquip.Size = New System.Drawing.Size(129, 20)
        Me.txtEquip.TabIndex = 36
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(13, 435)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(144, 17)
        Me.Label20.TabIndex = 35
        Me.Label20.Text = "Equip (Weapon) --"
        '
        'txtNo
        '
        Me.txtNo.BackColor = System.Drawing.Color.Black
        Me.txtNo.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNo.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNo.ForeColor = System.Drawing.Color.White
        Me.txtNo.Location = New System.Drawing.Point(173, 560)
        Me.txtNo.MaxLength = 1
        Me.txtNo.Name = "txtNo"
        Me.txtNo.Size = New System.Drawing.Size(129, 20)
        Me.txtNo.TabIndex = 44
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(13, 561)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(144, 17)
        Me.Label21.TabIndex = 43
        Me.Label21.Text = "No --------------"
        '
        'txtIInspect
        '
        Me.txtIInspect.BackColor = System.Drawing.Color.Black
        Me.txtIInspect.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtIInspect.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIInspect.ForeColor = System.Drawing.Color.White
        Me.txtIInspect.Location = New System.Drawing.Point(173, 485)
        Me.txtIInspect.MaxLength = 1
        Me.txtIInspect.Name = "txtIInspect"
        Me.txtIInspect.Size = New System.Drawing.Size(129, 20)
        Me.txtIInspect.TabIndex = 46
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(13, 486)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(144, 17)
        Me.Label22.TabIndex = 45
        Me.Label22.Text = "Inspect Item ----"
        '
        'txtSelectionKey11
        '
        Me.txtSelectionKey11.BackColor = System.Drawing.Color.Black
        Me.txtSelectionKey11.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSelectionKey11.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionKey11.ForeColor = System.Drawing.Color.White
        Me.txtSelectionKey11.Location = New System.Drawing.Point(491, 269)
        Me.txtSelectionKey11.MaxLength = 1
        Me.txtSelectionKey11.Name = "txtSelectionKey11"
        Me.txtSelectionKey11.Size = New System.Drawing.Size(129, 20)
        Me.txtSelectionKey11.TabIndex = 68
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.White
        Me.Label23.Location = New System.Drawing.Point(331, 272)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(109, 13)
        Me.Label23.TabIndex = 67
        Me.Label23.Text = "Selection 11 ----"
        '
        'txtSelectionKey10
        '
        Me.txtSelectionKey10.BackColor = System.Drawing.Color.Black
        Me.txtSelectionKey10.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSelectionKey10.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionKey10.ForeColor = System.Drawing.Color.White
        Me.txtSelectionKey10.Location = New System.Drawing.Point(491, 243)
        Me.txtSelectionKey10.MaxLength = 1
        Me.txtSelectionKey10.Name = "txtSelectionKey10"
        Me.txtSelectionKey10.Size = New System.Drawing.Size(129, 20)
        Me.txtSelectionKey10.TabIndex = 66
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.White
        Me.Label24.Location = New System.Drawing.Point(331, 246)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(109, 13)
        Me.Label24.TabIndex = 65
        Me.Label24.Text = "Selection 10 ----"
        '
        'txtSelectionKey9
        '
        Me.txtSelectionKey9.BackColor = System.Drawing.Color.Black
        Me.txtSelectionKey9.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSelectionKey9.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionKey9.ForeColor = System.Drawing.Color.White
        Me.txtSelectionKey9.Location = New System.Drawing.Point(491, 217)
        Me.txtSelectionKey9.MaxLength = 1
        Me.txtSelectionKey9.Name = "txtSelectionKey9"
        Me.txtSelectionKey9.Size = New System.Drawing.Size(129, 20)
        Me.txtSelectionKey9.TabIndex = 64
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.White
        Me.Label25.Location = New System.Drawing.Point(331, 220)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(109, 13)
        Me.Label25.TabIndex = 63
        Me.Label25.Text = "Selection 9 -----"
        '
        'txtSelectionKey8
        '
        Me.txtSelectionKey8.BackColor = System.Drawing.Color.Black
        Me.txtSelectionKey8.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSelectionKey8.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionKey8.ForeColor = System.Drawing.Color.White
        Me.txtSelectionKey8.Location = New System.Drawing.Point(491, 192)
        Me.txtSelectionKey8.MaxLength = 1
        Me.txtSelectionKey8.Name = "txtSelectionKey8"
        Me.txtSelectionKey8.Size = New System.Drawing.Size(129, 20)
        Me.txtSelectionKey8.TabIndex = 62
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.White
        Me.Label26.Location = New System.Drawing.Point(331, 195)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(109, 13)
        Me.Label26.TabIndex = 61
        Me.Label26.Text = "Selection 8 -----"
        '
        'txtSelectionKey7
        '
        Me.txtSelectionKey7.BackColor = System.Drawing.Color.Black
        Me.txtSelectionKey7.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSelectionKey7.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionKey7.ForeColor = System.Drawing.Color.White
        Me.txtSelectionKey7.Location = New System.Drawing.Point(491, 166)
        Me.txtSelectionKey7.MaxLength = 1
        Me.txtSelectionKey7.Name = "txtSelectionKey7"
        Me.txtSelectionKey7.Size = New System.Drawing.Size(129, 20)
        Me.txtSelectionKey7.TabIndex = 60
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.White
        Me.Label27.Location = New System.Drawing.Point(331, 169)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(109, 13)
        Me.Label27.TabIndex = 59
        Me.Label27.Text = "Selection 7 -----"
        '
        'txtSelectionKey6
        '
        Me.txtSelectionKey6.BackColor = System.Drawing.Color.Black
        Me.txtSelectionKey6.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSelectionKey6.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionKey6.ForeColor = System.Drawing.Color.White
        Me.txtSelectionKey6.Location = New System.Drawing.Point(491, 140)
        Me.txtSelectionKey6.MaxLength = 1
        Me.txtSelectionKey6.Name = "txtSelectionKey6"
        Me.txtSelectionKey6.Size = New System.Drawing.Size(129, 20)
        Me.txtSelectionKey6.TabIndex = 58
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.White
        Me.Label28.Location = New System.Drawing.Point(331, 143)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(109, 13)
        Me.Label28.TabIndex = 57
        Me.Label28.Text = "Selection 6 -----"
        '
        'txtSelectionKey5
        '
        Me.txtSelectionKey5.BackColor = System.Drawing.Color.Black
        Me.txtSelectionKey5.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSelectionKey5.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionKey5.ForeColor = System.Drawing.Color.White
        Me.txtSelectionKey5.Location = New System.Drawing.Point(491, 114)
        Me.txtSelectionKey5.MaxLength = 1
        Me.txtSelectionKey5.Name = "txtSelectionKey5"
        Me.txtSelectionKey5.Size = New System.Drawing.Size(129, 20)
        Me.txtSelectionKey5.TabIndex = 56
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.White
        Me.Label29.Location = New System.Drawing.Point(331, 117)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(109, 13)
        Me.Label29.TabIndex = 55
        Me.Label29.Text = "Selection 5 -----"
        '
        'txtSelectionKey4
        '
        Me.txtSelectionKey4.BackColor = System.Drawing.Color.Black
        Me.txtSelectionKey4.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSelectionKey4.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionKey4.ForeColor = System.Drawing.Color.White
        Me.txtSelectionKey4.Location = New System.Drawing.Point(491, 88)
        Me.txtSelectionKey4.MaxLength = 1
        Me.txtSelectionKey4.Name = "txtSelectionKey4"
        Me.txtSelectionKey4.Size = New System.Drawing.Size(129, 20)
        Me.txtSelectionKey4.TabIndex = 54
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.White
        Me.Label30.Location = New System.Drawing.Point(331, 91)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(109, 13)
        Me.Label30.TabIndex = 53
        Me.Label30.Text = "Selection 4 -----"
        '
        'txtSelectionKey3
        '
        Me.txtSelectionKey3.BackColor = System.Drawing.Color.Black
        Me.txtSelectionKey3.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSelectionKey3.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionKey3.ForeColor = System.Drawing.Color.White
        Me.txtSelectionKey3.Location = New System.Drawing.Point(491, 62)
        Me.txtSelectionKey3.MaxLength = 1
        Me.txtSelectionKey3.Name = "txtSelectionKey3"
        Me.txtSelectionKey3.Size = New System.Drawing.Size(129, 20)
        Me.txtSelectionKey3.TabIndex = 52
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.White
        Me.Label31.Location = New System.Drawing.Point(331, 65)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(109, 13)
        Me.Label31.TabIndex = 51
        Me.Label31.Text = "Selection 3 -----"
        '
        'txtSelectionKey2
        '
        Me.txtSelectionKey2.BackColor = System.Drawing.Color.Black
        Me.txtSelectionKey2.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSelectionKey2.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionKey2.ForeColor = System.Drawing.Color.White
        Me.txtSelectionKey2.Location = New System.Drawing.Point(491, 36)
        Me.txtSelectionKey2.MaxLength = 1
        Me.txtSelectionKey2.Name = "txtSelectionKey2"
        Me.txtSelectionKey2.Size = New System.Drawing.Size(129, 20)
        Me.txtSelectionKey2.TabIndex = 50
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.White
        Me.Label32.Location = New System.Drawing.Point(331, 39)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(109, 13)
        Me.Label32.TabIndex = 49
        Me.Label32.Text = "Selection 2 -----"
        '
        'txtSelectionKey1
        '
        Me.txtSelectionKey1.BackColor = System.Drawing.Color.Black
        Me.txtSelectionKey1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSelectionKey1.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionKey1.ForeColor = System.Drawing.Color.White
        Me.txtSelectionKey1.Location = New System.Drawing.Point(491, 10)
        Me.txtSelectionKey1.MaxLength = 1
        Me.txtSelectionKey1.Name = "txtSelectionKey1"
        Me.txtSelectionKey1.Size = New System.Drawing.Size(129, 20)
        Me.txtSelectionKey1.TabIndex = 48
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Consolas", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.White
        Me.Label33.Location = New System.Drawing.Point(331, 13)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(109, 13)
        Me.Label33.TabIndex = 47
        Me.Label33.Text = "Selection 1 -----"
        '
        'Controls
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(313, 660)
        Me.Controls.Add(Me.txtSelectionKey11)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.txtSelectionKey10)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.txtSelectionKey9)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.txtSelectionKey8)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.txtSelectionKey7)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.txtSelectionKey6)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.txtSelectionKey5)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.txtSelectionKey4)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.txtSelectionKey3)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.txtSelectionKey2)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.txtSelectionKey1)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.txtIInspect)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.txtNo)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.txtYes)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtEat)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.txtSInspect)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.txtEquip)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.txtWear2)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtWear1)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtShop)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtUse)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtDrink)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.txtRun)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.txtWait)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.txtSpec)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.txtMagic)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtAttack)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtTalk)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtInpect)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtRight)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtLeft)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDown)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtUp)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Controls"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Controls"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtUp As System.Windows.Forms.TextBox
    Friend WithEvents txtDown As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtLeft As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtRight As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtMagic As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtAttack As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtTalk As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtInpect As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtWear2 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtWear1 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtShop As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtUse As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtDrink As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtRun As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtWait As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtSpec As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtYes As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtEat As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtSInspect As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtEquip As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtNo As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtIInspect As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtSelectionKey11 As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtSelectionKey10 As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtSelectionKey9 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtSelectionKey8 As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtSelectionKey7 As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtSelectionKey6 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtSelectionKey5 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtSelectionKey4 As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtSelectionKey3 As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtSelectionKey2 As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtSelectionKey1 As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
End Class